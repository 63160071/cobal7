       IDENTIFICATION DIVISION. 
       PROGRAM-ID. HELLORPT.
       AUTHOR. Cindy.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT 200-OUTPUT-REPORT ASSIGN TO "HELLO.RPT"
              ORGANIZATION IS LINE SEQUENTIAL
      *  บรรทัด 9 ใส่คำว่าLINEเพื่อให้เคาะบรรทัดลงมา
      *  ถ้าไม่ใส่จะยาวเป็นบรรทัดเดียว
              FILE STATUS IS WS-OUTPUT-REPORT.
       DATA DIVISION. 
       FILE SECTION. 
       FD  200-OUTPUT-REPORT
           BLOCK CONTAINS 0 RECORDS.
       01  200-OUTPUT-REPORT-RECORD            PIC X(80).

       WORKING-STORAGE SECTION.
       01  WS-OUTPUT-REPORT                    PIC X(2).
           88 FTLE-OK                          VALUE '00'.
           88 FILE-AT-END                      VALUE '10'.
       01  WS-HEADER-REPORT                    PIC X(9)
           VALUE "# MESSAGE".
      *    สร้างHeader เพื่อใส่ข้อความ  # MESSAGE
       01  WS-LINE-REPORT.
           05 WS-SEQ                           PIC 9(1) VALUE 0.
           05 FILLER                           PIC X(1) VALUE SPACE.
      *    ใส่  FILLER เพื่อเว้นช่องว่างใส่ value spaceเพื่อให้เว้นบรรทัดลงมา   
           05 WS-MESSAGE                       PIC X(50). 

       PROCEDURE DIVISION.
       0000-MAIN-PROGRAM.
           OPEN OUTPUT 200-OUTPUT-REPORT 
           
           MOVE SPACE TO 200-OUTPUT-REPORT-RECORD 
           INITIALIZE 200-OUTPUT-REPORT-RECORD 
      * บรรทัด36-37 ใส่เพื่อเคียร์ของใน 200-OUTPUT-REPORT-RECORD ก่อน
      * เพื่อให้มั่นใจว่ามันว่างแต่ปกติแล้วในวินโด้จะว่างอยู่แล้ว
      * ควรทำทุกครั้งตอนเขียนเพิ่ม
           MOVE WS-HEADER-REPORT TO 200-OUTPUT-REPORT-RECORD 
           WRITE 200-OUTPUT-REPORT-RECORD 
           
           MOVE SPACE TO 200-OUTPUT-REPORT-RECORD 
           INITIALIZE 200-OUTPUT-REPORT-RECORD 
           ADD 1 TO WS-SEQ
      *    เพิ่ม 1 เข้า WS-SEQ ตลอดเพื่อทำ 1. 2. ข้างหน้า                       
           MOVE "HELLO" TO WS-MESSAGE 
           MOVE  WS-LINE-REPORT TO 200-OUTPUT-REPORT-RECORD 
           WRITE 200-OUTPUT-REPORT-RECORD
           
           MOVE SPACE TO 200-OUTPUT-REPORT-RECORD 
           INITIALIZE 200-OUTPUT-REPORT-RECORD 
           ADD 1 TO WS-SEQ
           MOVE "CS" TO WS-MESSAGE 
           MOVE  WS-LINE-REPORT TO 200-OUTPUT-REPORT-RECORD 
           WRITE 200-OUTPUT-REPORT-RECORD
           
           MOVE SPACE TO 200-OUTPUT-REPORT-RECORD 
           INITIALIZE 200-OUTPUT-REPORT-RECORD 
           ADD 1 TO WS-SEQ
           MOVE "INFORMATICS" TO WS-MESSAGE 
           MOVE  WS-LINE-REPORT TO 200-OUTPUT-REPORT-RECORD 
           WRITE 200-OUTPUT-REPORT-RECORD
           

           ADD 1 TO WS-SEQ
           MOVE "BURAPHA" TO WS-MESSAGE 
           MOVE  WS-LINE-REPORT TO 200-OUTPUT-REPORT-RECORD 
           WRITE 200-OUTPUT-REPORT-RECORD

           CLOSE 200-OUTPUT-REPORT 
           GOBACK 
           .